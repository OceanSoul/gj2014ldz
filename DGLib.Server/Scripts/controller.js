﻿var connection = $.hubConnection();
var proxy = connection.createHubProxy('GameHub');
proxy.on('PrintMessage', function (name, message) {
    console.log(name + ' ' + message);
    var date = new Date($.now());
    var formatedDate = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    $('#messageBucket').prepend('<span>' + formatedDate + '  <b>' + name + ':</b>' + ' ' + message + '</span></br>');
});
connection.start().done(function () {
    $('#newMessageButton').click(function () {
        if ($.trim($('#message').val()).length !== 0) {
            proxy.invoke('SendMessage', $('#displayname').val(), $('#message').val().substring(0, 20));
            $('#message').val('').focus();
        }
    });
    $("#message").keypress(function (e) {
        if (e.charCode == 13)
            $('#newMessageButton').trigger('click');
    });
    $("[id=command").click(function () {
        $('#message').val($(this).html().trim());
    });
});