﻿using System.Collections.Generic;
using Microsoft.AspNet.SignalR;

namespace DGLib.Server
{
    public class GameHub : Hub
    {
        public void SendMessage(string name, string message)
        {
            Clients.All.PrintMessage(name, message.Replace("<","").Replace(">",""));
        }
    }
}