﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UpdateSignal : MonoBehaviour {
	 	// Use this for initialization
	void Start () {
		var x = GameObject.Find("SignalR").GetComponent<MessagesController>();
		x.UIText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
