﻿using UnityEngine;
using System.Collections;

public class ServerInteraction : MonoBehaviour {
	public int holder;
	public Vector3 spawnPosition;
	public GameObject blackHole;
	public GameObject deguDegu;
    public CameraFucker CameraFucker;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

		//if(Input.GetKey(KeyCode.Space)) 
		//	deguDegu.GetComponent<DeguBehavior>().axisX = 1;
		switch(holder){
		case 1:{
			GameObject cloneDarkHole = (GameObject) Instantiate(blackHole, spawnPosition, new Quaternion (0,0,0,0));
			holder = 0;
			break;
		}
		case 2:{
				deguDegu.GetComponent<DeguBehavior>().ReverseSteering();
				break;
			}
		
		case 4:{
			deguDegu.GetComponent<DeguBehavior>().axisX = 1;
			break;
		}
		case 5:{
			deguDegu.GetComponent<DeguBehavior>().axisX = -1;
			break;
		}
		case 6:{
			deguDegu.GetComponent<DeguBehavior>().axisY = 1;
			break;
		}
		case 7:{
			deguDegu.GetComponent<DeguBehavior>().axisY = -1;
			break;
		}
        case 3:
		    {
		        CameraFucker.StartShaker = true;
                holder = 0;
                break;

           }
	}
	}}

