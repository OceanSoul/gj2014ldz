﻿using System;
using UnityEngine;
using System.Collections;
using Random = System.Random;

public class CameraFucker : MonoBehaviour
{

    public bool StartShaker;
    private Vector3 _origin;
    public float MaxDiff = 3f;
    private float _currentTime = 0;
    // Use this for initialization
	void Start ()
	{
	    _origin = Camera.main.transform.position;
	}

    // Update is called once per frame
	void Update () {

	    if (StartShaker)
	    {
	        var random = new Random(Guid.NewGuid().GetHashCode());
            Camera.main.transform.position += new Vector3((float)random.NextDouble(), (float)random.NextDouble(), _origin.z);
	        if (Math.Abs(Camera.main.transform.position.x - _origin.x) > MaxDiff)
                Camera.main.transform.position = new Vector3(_origin.x, Camera.main.transform.position.y, _origin.z);
            if (Math.Abs(Camera.main.transform.position.y - _origin.y) > MaxDiff)
                Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, _origin.y, _origin.z);
	        _currentTime += Time.deltaTime;
	        if (_currentTime > 2f)
	        {
	            StartShaker = false;
    	        _currentTime = 0;
	        }
	    }
	    else
	    {
	        Camera.main.transform.position = _origin;
	    }
	}
}
