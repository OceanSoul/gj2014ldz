﻿using UnityEngine;
using System.Collections;

public class MapBoundry : MonoBehaviour {
	public GameObject portal;
	public int strengthSubstraction;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		var x = other.GetComponent<BitsPrototype>();
		if(x != null)
		{
			if(other.name.Contains("ShieldBit"))
			{
				Destroy(other.gameObject);
				return;
			}
			other.gameObject.transform.position = new Vector3(other.transform.position.x, portal.transform.position.y, other.gameObject.transform.position.z);
			other.GetComponent<BitsPrototype>().strenght -= strengthSubstraction;
			//float temp = 90 - other.GetComponent<BitsPrototype>().transform.rotation.z;
			//x.moveVector = new Vector3(x.moveVector.x, x.moveVector.y + temp + temp, x.moveVector.z);
			//Debug.Log(x.moveVector); 
			//float  temp = 360 - other.GetComponent<BitsPrototype>().transform.rotation.z;
			//x.moveVector = new Vector3(x.moveVector.x,x.moveVector.y, other.GetComponent<BitsPrototype>().transform.rotation.z  + x.moveVector.z - transform.rotation.z);
			//Debug.Log(x.moveVector); 
		}
	}
}
