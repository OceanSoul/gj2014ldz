﻿using UnityEngine;
using System.Collections;

public class BlackHoleBehaviour : MonoBehaviour {
	public float rotationSpeed;
	public float maxExistenceTime;
	public float currentTime = 0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		currentTime += Time.deltaTime;
		if(currentTime > maxExistenceTime) Destroy(this.gameObject);
		//transform.rotation.eulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y,transform.rotation.eulerAngles.z+rotationSpeed*Time.deltaTime);
		transform.Rotate(new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y,transform.rotation.eulerAngles.z+rotationSpeed*Time.deltaTime));
	}

	void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Wave"){
			Destroy(other.gameObject);
		}
	}
}
