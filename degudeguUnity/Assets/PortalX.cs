﻿using UnityEngine;
using System.Collections;

public class PortalX : MonoBehaviour {
	public GameObject portal;
	public int strengthSubstraction;
	public bool isLeft;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerEnter(Collider other){
		Debug.Log("Trig");
		if(other.gameObject.tag == "Degu")
			other.gameObject.transform.position = new Vector3(portal.transform.position.x +(isLeft? 2 : -2),
			                                                  other.gameObject.transform.position.y,
			                                                  other.gameObject.transform.position.z);
		}
	}

