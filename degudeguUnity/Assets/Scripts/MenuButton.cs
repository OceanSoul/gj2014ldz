﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class MenuButton: MonoBehaviour {

	public Color kolorek;
	public bool isStart;
	void Start () {

		var entry = new EventTrigger.Entry();
		entry.eventID = EventTriggerType.PointerEnter;
		entry.callback.AddListener (Enter);

		var eventTrigger = gameObject.GetComponent<EventTrigger>();
		eventTrigger.delegates.Add (entry); 
		 

		var entry_1 = new EventTrigger.Entry();
		entry_1.eventID = EventTriggerType.PointerExit;
		entry_1.callback.AddListener (Quit);

		eventTrigger.delegates.Add (entry_1);

		var entry_2 = new EventTrigger.Entry();
		entry_2.eventID = EventTriggerType.PointerClick;
		entry_2.callback.AddListener (Load);
		
		eventTrigger.delegates.Add (entry_2);


	}
    void Enter(BaseEventData data){
		gameObject.GetComponent<Text> ().color = kolorek;
		}

	void Quit(BaseEventData data){
		gameObject.GetComponent<Text> ().color = Color.black;
	}
		
	void Load(BaseEventData data){
			if (isStart) {

						Application.LoadLevel (03);
				} else {
						Application.Quit ();
				}
	}
		
}

