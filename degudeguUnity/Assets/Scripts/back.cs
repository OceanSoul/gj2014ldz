﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class back : MonoBehaviour {
	public Color kolor;
	// Use this for initialization
	void Start () {
		var entry = new EventTrigger.Entry();
		entry.eventID = EventTriggerType.PointerEnter;
		entry.callback.AddListener (Enter);
		
		var eventTrigger = gameObject.GetComponent<EventTrigger>();
		eventTrigger.delegates.Add (entry); 

		var entry_2 = new EventTrigger.Entry();
		entry_2.eventID = EventTriggerType.PointerExit;
		entry_2.callback.AddListener (Quit);

		eventTrigger.delegates.Add (entry_2); 

		var entry_3 = new EventTrigger.Entry();
		entry_3.eventID = EventTriggerType.PointerClick;
		entry_3.callback.AddListener (BCK);
		
		eventTrigger.delegates.Add (entry_3); 
	}
	
	// Update is called once per frame
	void Enter (BaseEventData data) {
		gameObject.GetComponent<Text> ().color = kolor;
	}

	void Quit (BaseEventData data) {
		gameObject.GetComponent<Text> ().color = Color.black;
	}
	void BCK (BaseEventData data) {
		Application.LoadLevel(01);
	}
}
