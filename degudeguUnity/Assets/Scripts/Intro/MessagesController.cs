﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DGLib;
using UnityEngine.UI;
using Random = System.Random;

public class MessagesController : MonoBehaviour
{
	public Text UIText;
	public float MovingTextLag = .1f;


	private SignalRClient _signalRClient;
    private List<ServerAction> _serverActions;
    private List<string> _messagesList;
	private MovingText _movingText;
	private float _currentTime = 0f;

	// Use this for initialization
	void Start ()
	{
		_movingText = new MovingText(120);
		_messagesList = new List<string>();
        _serverActions = new List<ServerAction>();
		_signalRClient = new SignalRClient();
		_signalRClient.NewMessage += NewMessage;
		_signalRClient.Start();
		DontDestroyOnLoad(this);
	}

	void NewMessage(string name, string message)
	{
	    lock (_messagesList)
		{
            _messagesList.Add(string.Format("<b>{0}</b>: {1}", name, message));
		}
	    InterpreteServerMessage(message);
	}

    private void InterpreteServerMessage(string message)
    {
		message = message.Trim();
		Debug.Log(message);
        var action = new ServerAction();
        if (message.ToLower() == "#blackhole")
        {
            action.Type = 1;
        }
        if (message.ToLower() == "#degu-up")
        {
            action.Type = 6;
        }
        if (message.ToLower() == "#degu-down")
        {
            action.Type = 7;
        }
        if (message.ToLower() == "#degu-left")
        {
            action.Type = 5;
        }
        if (message.ToLower() == "#degu-right")
        {
            action.Type = 4;
        }
        if (message.ToLower() == "#camera")
        {
            action.Type = 3;
        }
        if (message.ToLower() == "#reverse")
        {
            action.Type = 2;
        }
        lock (_serverActions)
        {
            _serverActions.Add(action);
        }
    }


    // Update is called once per frame
	void Update ()
	{
		_currentTime += Time.deltaTime;		
		lock (_messagesList)
		{
			if (_messagesList.Any())
			{
				_movingText.PushString(_messagesList.First()); 
				_messagesList.RemoveAt(0);
			}
		}
	    lock (_serverActions)
	    {
            if (_serverActions.Any())
            {
                SendActionToGame(_serverActions.ElementAt(0));
                _serverActions.RemoveAt(0);
            }
	    }
		if (_currentTime >= MovingTextLag)
		{
			_currentTime = 0f;
			UpdateMovingText();
		}
	}

    private void SendActionToGame(ServerAction action)
    {
        Debug.Log(action.Type);
        var interactor = FindObjectOfType<ServerInteraction>();
        if (interactor != null)
        {
            interactor.holder = action.Type;
            interactor.spawnPosition = action.Position;
        }
    }

    private void UpdateMovingText()
	{
		_movingText.Shift();
		UIText.text = _movingText.GetString();
	}

    internal class ServerAction
    {
        private readonly Vector3[] _randomVectors =
        { // x = -7 do 7
		  // y = -5 do 5	
            new Vector3(-3, 4),
            new Vector3(6, -2),
            new Vector3(7, -5),
            new Vector3(0, 0),
            new Vector3(1, 2),
            new Vector3(1, 4),
			new Vector3(-4, 5),
			new Vector3(3, 3),
			new Vector3(1, 1),
			new Vector3(4, -1),
			new Vector3(-1, -2),
			new Vector3(-5, -3),

        };
        public ServerAction()
        {
            Position = _randomVectors[new Random(Guid.NewGuid().GetHashCode()).Next(0, _randomVectors.Length)];
        }
        public int Type { get; set; }
        public Vector3 Position { get; set; }
    }
}
