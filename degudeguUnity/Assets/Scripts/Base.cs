﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Base : MonoBehaviour {
	//Player Stats
	public float currency;
	public float health;
	public int controlingPlayer; //sets controlling player - mainly for input

	//Spawner settings
	public float rotatingSpeed;
	public Transform spawnerRotation;
	//Waves spawning stats
	public float[] cooldown; // time beetween spawns - const
	public float[] remainingCooldown; // current time needed for spawn
	public float[] waveCost; // each wave cost
	//GameObjects for Waves
	public GameObject firstWaveType;
	public GameObject secondWaveType;
	public GameObject thirdWaveType;
	public GameObject fourthWaveType;
	public GameObject fifthWaveType;

	public GameObject healthText;
	public GameObject currencyText;
	private AudioSource audio;

	public Material player1Mat;
	public Material player2Mat;



	// Use this for initialization
	void Start () {
		spawnerRotation = this.transform.FindChild("RotationPoint" + controlingPlayer).transform;
		Debug.Log("KOT");
		audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		healthText.GetComponent<Text>().text = "HP: " + health.ToString();
		currencyText.GetComponent<Text>().text = string.Format("HZ: {0:0}", currency);
		if(health <= 0) GameOver();
		AddmHz();
		//Rotate spawningPoint counter clockwise
		for(int x=0;x<remainingCooldown.Length;++x){
			remainingCooldown[x]+=Time.deltaTime;
		}

		if(Input.GetButton(getPlayerIdentifier() + "RotateLeft")){
			spawnerRotation.Rotate(Vector3.back * -rotatingSpeed);
		}
		//Rotate spawningPoint clockwise
		if(Input.GetButton(getPlayerIdentifier() + "RotateRight")){
			spawnerRotation.Rotate(Vector3.back * rotatingSpeed);
		}
		if(Input.GetButton(getPlayerIdentifier()+ "SpawnFirst") && noCooldown(0) && haveEnoughHz(0)){
			//Spawn FirstWaveType
			//Current test for cone shaped wave
			remainingCooldown[0]=0;
			GameObject cloneWaveOne = (GameObject) Instantiate(firstWaveType,transform.position,spawnerRotation.transform.rotation);
			cloneWaveOne.gameObject.layer = (controlingPlayer == 1) ? 9 : 11;
			cloneWaveOne.GetComponent<WavePrototype>().startingRotation = spawnerRotation.transform.rotation;
			cloneWaveOne.GetComponent<WavePrototype>().material = (controlingPlayer == 1 ? player1Mat : player2Mat);
			currency -= waveCost[0];;
			audio.Play();
		}
		if(Input.GetButton(getPlayerIdentifier()+ "SpawnSecond") && noCooldown(1) && haveEnoughHz(1)){
			//Spawn SecondWaveType
			//Current test for omni directional wave
			remainingCooldown[1]=0;
			GameObject cloneWaveTwo = (GameObject) Instantiate(secondWaveType,transform.position,spawnerRotation.transform.rotation);
			cloneWaveTwo.gameObject.layer = (controlingPlayer == 1) ? 9 : 11;
			cloneWaveTwo.GetComponent<WavePrototype>().startingRotation = spawnerRotation.transform.rotation;
			cloneWaveTwo.GetComponent<WavePrototype>().material = (controlingPlayer == 1 ? player1Mat : player2Mat);
			currency -= waveCost[1];
			audio.Play();
		}
		if(Input.GetButton(getPlayerIdentifier()+ "SpawnThird") && noCooldown(2) && haveEnoughHz(2)){
			//Spawn ThirdWaveType
			remainingCooldown[2]=0;
			GameObject cloneWaveThree = (GameObject) Instantiate(thirdWaveType,transform.position,spawnerRotation.transform.rotation);
			cloneWaveThree.gameObject.layer = (controlingPlayer == 1) ? 9 : 11;
			cloneWaveThree.GetComponent<WavePrototype>().startingRotation = spawnerRotation.transform.rotation;
			cloneWaveThree.GetComponent<WavePrototype>().material = (controlingPlayer == 1 ? player1Mat : player2Mat);
			currency -= waveCost[2];
			audio.Play();
		}
		/*
		if(Input.GetButton(getPlayerIdentifier()+ "SpawnFourth") && noCooldown(4) && haveEnoughHz(4)){
			//Spawn FourthWaveType
		}
		if(Input.GetButton(getPlayerIdentifier()+ "SpawnFifth") && noCooldown(5) && haveEnoughHz(5)){
			//Spawn FifthWaveType
		}
		*/
	}
	//Modifier for easier Input.GetButton() settings
	string getPlayerIdentifier(){
		return controlingPlayer==1 ? "PlayerOne" :"PlayerTwo";
	}
	//check if wave is not on cooldown
	bool noCooldown(int identifier){
		return (cooldown[identifier] - remainingCooldown[identifier]<0);
	}
	//check for enough mHz
	bool haveEnoughHz(int identifier){
		return(currency-waveCost[identifier] >= 0);
	}

	void AddmHz(){
		currency+=2*Time.deltaTime;
	}

	void GameOver(){
		Destroy(this.gameObject);
		GameData.EndingText = "Player " + (controlingPlayer == 1 ? "Two" : "One") + " Wins!!!";
		Application.LoadLevel("04");
	}

	void onTriggerEnter(Collider col)
	{
		Debug.Log(col);
	}
}
