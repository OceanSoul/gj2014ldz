﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WavePrototype : MonoBehaviour {
	public GameObject waveBit;
	public float waveDegreeIncrease;
	public int waveDegreeWidth;
	public float bitSpeed;
	public float bitSizeIncrease;
	public int bitStrenght;
	public float wavedegreePrecision;
	public float maxResizeTime;
	public List<GameObject> bits;
	public Quaternion startingRotation;

	public Material material;

	// Use this for initialization
	void Start () {

		for(float num=-(float)waveDegreeWidth/2;num < waveDegreeWidth/2;num+=wavedegreePrecision){
			//Vector3 newRotation = new Vector3(startingRotation);
			//Debug.Log(newRotation.z + "   " + transform.rotation.z);
			//Quaternion tempQuaternion = Quaternion.Euler(newRotation);
			//Debug.Log(num);
			Quaternion bitRotation = new Quaternion();
			bitRotation.eulerAngles = new Vector3(startingRotation.eulerAngles.x, startingRotation.eulerAngles.y, startingRotation.eulerAngles.z - (num*waveDegreeIncrease) );
			//Debug.Log(num + "    " + bitRotation.eulerAngles.z + "   "+ waveDegreeIncrease);

			GameObject clone = (GameObject) Instantiate(waveBit,transform.position, bitRotation); 
			clone.GetComponent<BitsPrototype>().maxSpeed = bitSpeed;
			clone.GetComponent<BitsPrototype>().strenght = bitStrenght;
			clone.GetComponent<BitsPrototype>().maxSizeIncrease = bitSizeIncrease;
			clone.GetComponent<BitsPrototype>().maxResizeTime = maxResizeTime;
			clone.transform.parent = transform;
			clone.layer = this.gameObject.layer;
			clone.renderer.material = material;
			bits.Add(clone);
		}

		//Debug.Log(transform.rotation);
	}
	
	// Update is called once per frame
	void Update () {
		//transform.localScale *= sizeIncreaseMod;
		//foreach(GameObject x in bits){
		//	x.transform.Translate(Vector3.forward * speed);
		//	x.transform.localScale = Vector3.right * sizeIncreaseMod;
		//}
	}
}
