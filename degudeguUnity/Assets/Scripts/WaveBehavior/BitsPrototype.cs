﻿using UnityEngine;
using System.Collections;

public class BitsPrototype : MonoBehaviour {
	[HideInInspector]
	public float maxSpeed;
	[HideInInspector]
	public float curSpeed;
	[HideInInspector]
	public float maxSizeIncrease;
	public float curSizeIncrease;
	public int strenght;
	public Vector3 moveVector;
	public float resizeTimer=0;
	public float maxResizeTime=5f;
	// Use this for initialization
	void Start () {
		OnStart();
		curSpeed = maxSpeed;
		curSizeIncrease = maxSizeIncrease;
		moveVector = Vector3.up;
	}
	
	protected virtual void OnStart() {}
	
	protected virtual void OnUpdate(){}


	// Update is called once per frame
	void Update () {
		resizeTimer += Time.deltaTime;
		transform.Translate(moveVector * curSpeed * Time.deltaTime);
		//transform.localScale.x *=(sizeIncreaseMod+1);
		if(resizeTimer < maxResizeTime)transform.localScale = new Vector3(transform.localScale.x * (curSizeIncrease+1), transform.localScale.y, transform.localScale.z);
		if(strenght <= 0) Destroy(this.gameObject);
		OnUpdate();
	}
	
	protected virtual void Die(){}
	//void OnTriggerEnter(Collider collision) {speed = 0f;}
	//if(other.gameObject.tag == "Player"){
}
