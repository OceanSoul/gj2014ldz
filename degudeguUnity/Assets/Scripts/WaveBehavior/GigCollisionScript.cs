﻿using UnityEngine;
using System.Collections;

public class GigCollisionScript : MonoBehaviour {
	public BitsPrototype movement;
	public int maxStrength = 100;
	// Use this for initialization
	void Start () {
		movement = this.GetComponent<BitsPrototype>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider other){
		//Debug.Log(other.gameObject.tag);
		switch(other.gameObject.tag)
		{
		case "Wave":{
			checked
			{
			BitsPrototype otherStats = other.GetComponent<BitsPrototype>();
			int temp = otherStats.strenght;
			otherStats.strenght = (otherStats.strenght - movement.strenght) %maxStrength;
			movement.strenght = (movement.strenght - temp) %maxStrength;
			}
			break;
		}

		case "Obstacle":{
			movement.curSpeed = movement.maxSpeed/2;
			movement.curSizeIncrease = movement.maxSizeIncrease/2;
			break;
		}
		case "Base":{
			other.GetComponent<Base>().health -= movement.strenght;
			Destroy(gameObject);
			break;
		}
		default:{
			break;}
		}
	}
	void OnTriggerStay(Collider other){

	}
	void OnTriggerExit(Collider other){
		movement.curSpeed = movement.maxSpeed;
		movement.curSizeIncrease = movement.maxSizeIncrease;
	}
}
