﻿using UnityEngine;
using System.Collections;

public class ShieldCollisionBehaviour : MonoBehaviour {
	public BitsPrototype movement;
	public float maxTime;
	public float currentTime;
	
	// Use this for initialization
	void Start () {
		movement = this.GetComponent<BitsPrototype>();
	}
	
	// Update is called once per frame
	void Update () {
		currentTime += Time.deltaTime;
		if(currentTime > maxTime) Destroy(this.gameObject);
	}

	void OnTriggerEnter(Collider other){
		Debug.Log("Trigger");
		switch(other.tag)
		{
		case "Wave":{
			Destroy(other.gameObject);
			break;
		}
		}
	}

	void OnCollisionEnter(Collision other){
		Debug.Log("cOLLISION");
		switch(other.gameObject.tag)
		{
		case "Wave":{
			Destroy(other.gameObject);
			Destroy(this.gameObject);
			break;
		}
		}
	}
}
