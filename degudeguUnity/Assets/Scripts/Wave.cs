﻿using UnityEngine;
using System.Collections;

public class Wave : MonoBehaviour {
	public float waveDegreeWidth;
	public float speed;
	public float sizeIncreaseMod;
	public float strenght;
	// Use this for initialization
	void Start () {
		OnStart();
	}

	protected virtual void OnStart() {}

	protected virtual void OnUpdate(){}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(Vector2.right * speed);
		transform.localScale *=(sizeIncreaseMod+1);
		if(strenght <= 0) Die();
		OnUpdate();
	}

	protected virtual void Die(){}

	//void OnTriggerEnter(Collider collision) {speed = 0f;}
		//if(other.gameObject.tag == "Player"){
			
		//}
}
