﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class push : MonoBehaviour {
	public EventTrigger eventTrigger;
	public Color kolor;
	// Use this for initialization
	void Start () {
		var entry_4 = new EventTrigger.Entry();
		entry_4.eventID = EventTriggerType.PointerDown;
		entry_4.callback.AddListener (on);

		eventTrigger.delegates.Add (entry_4);

		var entry_5 = new EventTrigger.Entry();
		entry_5.eventID = EventTriggerType.PointerClick;
		entry_5.callback.AddListener (off);
		
		eventTrigger.delegates.Add (entry_5);

		var entry_6 = new EventTrigger.Entry();
		entry_6.eventID = EventTriggerType.PointerExit;
		entry_6.callback.AddListener (off);
		
		eventTrigger.delegates.Add (entry_6);
	}
	
	// Update is called once per frame
	void on (BaseEventData data) {
		gameObject.GetComponent<Image> ().color = kolor;

	}
	void off (BaseEventData data){
		gameObject.GetComponent<Image> ().color = new Vector4(0,0,0,0);

	}
}
