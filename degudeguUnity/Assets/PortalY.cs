﻿using UnityEngine;
using System.Collections;

public class PortalY : MonoBehaviour {
	public GameObject portal;
	public int strengthSubstraction;
	public bool isDown;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Degu")
			other.gameObject.transform.position = new Vector3(other.gameObject.transform.position.x,
		                                                                                    portal.transform.position.y+(isDown? 2 : -2),
		                                                                                    other.gameObject.transform.position.z);
	}
}

