﻿using UnityEngine;
using System.Collections;

public class DeguBehavior : MonoBehaviour {
	public int axisX;
	public int axisY;
	public bool isReversed=false;
	public float speed;
	public float timeBeforeNormalSteering;
	public float curTimer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log (axisX +"    "+ axisY);
		curTimer += Time.deltaTime;
		if(isReversed && curTimer > timeBeforeNormalSteering) isReversed = false;
		var x = transform.position.x + (axisX * (isReversed ? -1:1) * speed * Time.deltaTime);
		var y = transform.position.y + (axisY * (isReversed ? -1:1) * speed * Time.deltaTime);
		transform.position = new Vector3(x,y, transform.position.z);
		//transform.position.y += axisY * (isReversed ? -1:1) * speed * Time.deltaTime;//transform.Translate( Vector3.right * axisX * (isReversed ? -1:1) * Vector3.up * speed * Time.deltaTime);
		//transform.Translate( Vector3.up * axisX * (isReversed ? -1:1) * Vector3.up * speed * Time.deltaTime);
	}

	public void ReverseSteering(){
		isReversed = !isReversed;
	}

	void OnTriggerEnter(Collider other){
		if(other.tag == "Wave") Destroy(other.gameObject);
	}

}
