﻿using System;
using Microsoft.AspNet.SignalR.Client;

namespace DGLib
{
    public class SignalRClient
    {
        private const string _hubName = "GameHub";
        private const string _connectionString = "http://dglibserver.azurewebsites.net/";
        private readonly IHubProxy _hubProxy;
        private readonly HubConnection _hubConnection;

        public delegate void MessageHandler(string name, string message);
        public event MessageHandler NewMessage;

        public SignalRClient()
        {
            _hubConnection = new HubConnection(_connectionString, true);
            _hubProxy = _hubConnection.CreateHubProxy(_hubName);
            _hubProxy.On("PrintMessage", (string name, string message) => InvokeEvent(name, message));
        }

        private void InvokeEvent(string name, string message)
        {
            if (NewMessage != null)
                NewMessage(name, message);
        }

        public void Start()
        {
            _hubConnection.Start().Wait();
        }

        public void SendMessage(string name, string message)
        {
            _hubProxy.Invoke("SendMessage", name, message);
        }
    }
}
