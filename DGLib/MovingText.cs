﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DGLib
{
    public class MovingText
    {
        private readonly int _maxCharSize;
        private readonly char[] _currentChars;
        private readonly Queue<char> _charsToAdd = new Queue<char>();

        public MovingText(int maxCharSize)
        {
            _maxCharSize = maxCharSize;
            _currentChars = new char[_maxCharSize];
            for (int i = 0; i < _maxCharSize; i++)
                _currentChars[i] = ' ';
        }

        public void PushString(string message)
        {
            foreach (var c in message)
            {
                _charsToAdd.Enqueue(c);                
            }
        }

        public void Shift()
        {
            for (int i = 0; i < _maxCharSize - 1; i++)
                _currentChars[i] = _currentChars[i + 1];
            _currentChars[_maxCharSize - 1] = GetNewChar();
        }

        public string GetString()
        {
            return FormatTags(RemoveClosingTags(new string(_currentChars))).PadRight(_maxCharSize - 10).PadLeft(10);
        }

        private string RemoveClosingTags(string s)
        {
            while (s != RemoveFirstBrokenTag(s))
            {
                s = RemoveFirstBrokenTag(s);
            }
            return s;
        }

        private string RemoveFirstBrokenTag(string s)
        {
            int i = 0;
            for (i = 0; i < s.Length; i++)
            {
                if (s[i] != ' ')
                    break;
            }

            if (s.Count(q => q == '>') == 1 && s.IndexOf('<') == -1)
            {
                s = s.Remove(0, s.IndexOf('>') + 1);
            }

            if (s.IndexOf('/') < s.IndexOf('>') && s.IndexOf('/') != -1)
            {
                if (s.IndexOf('<') < s.IndexOf('/'))
                {
                    s = s.Remove(s.IndexOf('<'), s.IndexOf('>') - s.IndexOf('<') + 1);
                }
                else
                {
                    s = s.Remove(s.IndexOf('/'), s.IndexOf('>') - s.IndexOf('/') + 1);
                }
            }

            var close = s.IndexOf('>');
            var open = s.IndexOf('<');
            if (close != -1 && open != -1 && close < open)
            {
                s = s.Remove(i, close - i + 1);
                open = s.IndexOf('<');
                close = s.IndexOf('>');
                return s.Remove(open, close - open + 1);
            }
            return s;
        }

        private string FormatTags(string s)
        {
            if (s.Count(q => q == '<') > s.Count(q => q == '>'))
            {
                if (s.Count(q => q == '<')%2 == 0)
                {
                    return FinishLastTag(s);
                }
                return RemoveTag(s);
            }
            if (s.Count(q => q == '<') % 2 == 1)
            {
                return FinishLastTag(s);
            }
            return s;
        }

        private string RemoveTag(string s)
        {
            return s.Remove(s.LastIndexOf('<'));
        }

        private string FinishLastTag(string s)
        {
            if (s.LastIndexOf('<') > s.LastIndexOf('>'))
            {
                s = s.Remove(s.LastIndexOf('<'));
            }
            var tag = GetLastTag(s);
            return s.TrimEnd() + tag;
        }

        private string GetLastTag(string s)
        {
            var start = s.LastIndexOf('<');
            var end = s.LastIndexOf('>');
            var tag = s.Substring(start, end - start + 1).Insert(1, "/");
            start = tag.LastIndexOf('=');
            if (start > 0)
            {
                end = tag.LastIndexOf('>');
                return tag.Remove(start, end - start);                
            }
            return tag;
        }

        private char GetNewChar()
        {
            if (_charsToAdd.Any())
                return _charsToAdd.Dequeue();
            return ' ';
        }

    }
}
