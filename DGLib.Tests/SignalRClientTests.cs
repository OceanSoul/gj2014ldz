﻿using System.Threading;
using NUnit.Framework;

namespace DGLib.Tests
{
    [TestFixture]
    class SignalRClientTests
    {
        [Test]
        public void Test()
        {
            bool test = false;
            var x = new SignalRClient();
            x.NewMessage += (name, message) => test = true;
            x.Start();
            x.SendMessage("a","a");
            Thread.Sleep(1000);
            Assert.IsTrue(test);
        }

        
    }
}
