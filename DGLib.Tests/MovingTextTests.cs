﻿using System.Diagnostics;
using NUnit.Framework;

namespace DGLib.Tests
{
    [TestFixture]
    class MovingTextTests
    {
        private string GetWhiteString(int x)
        {
            string s = string.Empty;
            for (int i = 0; i < x; i++)
                s += " ";
            return s;

        }

        [Test]
        public void Case1()
        {
            var txt = new MovingText(100);
            txt.PushString("text");
            Shift(txt, 50);
            var expect = "text";
            Debug.WriteLine(txt.GetString());
            Assert.AreEqual(expect, txt.GetString().Trim());
        }

        [Test]
        public void Case2()
        {
            var txt = new MovingText(100);
            string test = "text i cos <";
            txt.PushString(test);
            Shift(txt, 50);
            var expect = "text i cos";
            Debug.WriteLine(txt.GetString());
            Assert.AreEqual(expect, txt.GetString().Trim());
        }

        [Test]
        public void Case2_1()
        {
            var txt = new MovingText(100);
            string test = "text i cos <asdasd";
            txt.PushString(test);
            Shift(txt, 50);
            var expect = "text i cos";
            Debug.WriteLine(txt.GetString());
            Assert.AreEqual(expect, txt.GetString().Trim());
        }

        [Test]
        public void Case3()
        {
            var txt = new MovingText(100);
            string test = "text i cos <tag>";
            txt.PushString(test);
            Shift(txt, 50);
            var expect = "text i cos <tag></tag>";
            Debug.WriteLine(txt.GetString());
            Assert.AreEqual(expect, txt.GetString().Trim());
        }

        [Test]
        public void Case3_1()
        {
            var txt = new MovingText(100);
            string test = "text i cos <tag>asdasdassadasd";
            txt.PushString(test);
            Shift(txt, 50);
            var expect = "text i cos <tag>asdasdassadasd</tag>";
            Debug.WriteLine(txt.GetString());
            Assert.AreEqual(expect, txt.GetString().Trim());
        }

        [Test]
        public void Case4()
        {
            var txt = new MovingText(100);
            string test = "text i cos <tag>asdasdassadasd<";
            txt.PushString(test);
            Shift(txt, 50);
            var expect = "text i cos <tag>asdasdassadasd</tag>";
            Debug.WriteLine(txt.GetString());
            Assert.AreEqual(expect, txt.GetString().Trim());
        }

        [Test]
        public void Case5()
        {
            var txt = new MovingText(100);
            string test = "text i cos <tag>asdasdassadasd</tag";
            txt.PushString(test);
            Shift(txt, 50);
            var expect = "text i cos <tag>asdasdassadasd</tag>";
            Debug.WriteLine(txt.GetString());
            Assert.AreEqual(expect, txt.GetString().Trim());
        }

        [Test]
        public void Case5_1()
        {
            var txt = new MovingText(100);
            string test = "text i cos <tag>asdasdassadasd</tag> asdasd";
            txt.PushString(test);
            Shift(txt, 50);
            var expect = "text i cos <tag>asdasdassadasd</tag> asdasd";
            Debug.WriteLine(txt.GetString());
            Assert.AreEqual(expect, txt.GetString().Trim());
        }
        [Test]
        public void Case5_2()
        {
            var txt = new MovingText(100);
            string test = "text i cos <tag=text>asdasdassadasd</";
            txt.PushString(test);
            Shift(txt, 50);
            var expect = "text i cos <tag=text>asdasdassadasd</tag>";
            Debug.WriteLine(txt.GetString());
            Assert.AreEqual(expect, txt.GetString().Trim());
        }
        [Test]
        public void Case5_3()
        {
            var txt = new MovingText(100);
            string test = "text i cos <tag=text>asdasdassadasd</sd";
            txt.PushString(test);
            Shift(txt, 50);
            var expect = "text i cos <tag=text>asdasdassadasd</tag>";
            Debug.WriteLine(txt.GetString());
            Assert.AreEqual(expect, txt.GetString().Trim());
        }
        [Test]
        public void Case5_4()
        {
            var txt = new MovingText(100);
            string test = "n>Listener</color>: 123123<b>(6:23:29 PM)</b> <color=green>Liste";
            txt.PushString(test);
            Shift(txt, 70);
            var expect = "Listener: 123123<b>(6:23:29 PM)</b> <color=green>Liste</color>";
            Debug.WriteLine(txt.GetString());
            Assert.AreEqual(expect, txt.GetString().Trim());
        }

        [Test]
        public void Case5_5()
        {
            var txt = new MovingText(100);
            string test = ":53 PM)</b> <color=green>Listener</color>: 342342<asdasd";
            txt.PushString(test);
            Shift(txt, 70);
            var expect = ":53 PM) <color=green>Listener</color>: 342342";
            Debug.WriteLine(txt.GetString());
            Assert.AreEqual(expect, txt.GetString().Trim());
        }

        [Test]
        public void Case5_6()
        {
            var txt = new MovingText(100);
            string test = "olor>: 1231231";
            txt.PushString(test);
            Shift(txt, 70);
            var expect = ": 1231231";
            Debug.WriteLine(txt.GetString());
            Assert.AreEqual(expect, txt.GetString().Trim());
        }

        [Test]
        public void Case5_7()
        {
            var txt = new MovingText(100);
            string test = "</b> Listener</color>: 2123312123";
            txt.PushString(test);
            Shift(txt, 50);
            var expect = "Listener: 2123312123";
            Debug.WriteLine(txt.GetString());
            Assert.AreEqual(expect, txt.GetString().Trim());
        }
        
        

        private void Shift(MovingText txt, int amount)
        {
            for (int i = 0; i < amount; i++)
                txt.Shift();
        }
    }
}
